package com.example.testproject

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import com.example.testproject.database.AppDatabase
import com.example.testproject.entity.Category
import com.example.testproject.entity.Task
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class DaoTest {
    private var appDatabase: AppDatabase? = null

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    companion object {
        val TAG = DaoTest::class.java.simpleName
    }

    @Before
    fun setup() {
        AppDatabase.TEST_MODE = true
        appDatabase = AppDatabase.getInstance(InstrumentationRegistry.getInstrumentation().context)
    }

    @After
    fun tearDown() {
        appDatabase?.close()
    }

    val testCategories = listOf(Category("testCategory", 0), Category("testCategory2", 0))
    val taskArray = listOf(Task("name1", Date(), testCategories[0], true, true),
        Task("name2", Date(), testCategories[0], true, true))

    @Test
    @Throws(Exception::class)
    fun writeAndReadTask() {
        appDatabase?.tasksDao()?.insertTask(taskArray[0])
        var newArray = appDatabase?.tasksDao()?.getAllTasks()
        assert(newArray != null && newArray.isNotEmpty())
        assertEquals(taskArray[0], newArray!![0])

        appDatabase?.tasksDao()?.insertTask(taskArray[1])
        newArray = appDatabase?.tasksDao()?.getAllTasks()
        assert(newArray!!.size == 2)
        assertEquals(taskArray, newArray)
    }

    @Test
    fun deletionTasks() {
        appDatabase?.tasksDao()?.removeTask(taskArray[1])
        assert(appDatabase?.tasksDao()?.getAllTasks()!!.size == 1)
    }

    @Test
    @Throws(Exception::class)
    fun writeAndReadCategory() {
        appDatabase?.categoriesDao()?.insertCategory(testCategories[0])
        val newArray = appDatabase?.categoriesDao()?.getAllCategories()
        assert(newArray != null && newArray.isNotEmpty())
        assertEquals(testCategories[0], newArray!![0])
    }
}