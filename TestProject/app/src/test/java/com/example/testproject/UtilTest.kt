package com.example.testproject

import com.example.testproject.utility.Util
import org.junit.Assert
import org.junit.Test
import java.util.*

class UtilTest {
    @Test
    fun testDateFormat() {
        val calendar = Calendar.getInstance()
        calendar.set(2020, 12, 10)
        Assert.assertEquals(Util.simpleDateFormat(calendar.time), "10-12-2020")
    }

    @Test
    fun testTaskSortByDate() {
    }
}