package com.example.testproject.database

import androidx.room.TypeConverter
import java.util.*

class RoomDateConverter {
    companion object {
        @TypeConverter
        @JvmStatic
        fun fromDate(date: Date): Long {
            return date.time
        }

        @TypeConverter
        @JvmStatic
        fun toDate(date: Long): Date {
            return Date(date)
        }
    }
}