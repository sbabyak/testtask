package com.example.testproject.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.testproject.R

//Simple spinner adapter to represent colored textview
class CategoryColorSpinnerAdapter(var context: Context) : BaseAdapter() {
    var colors: ArrayList<Int> = ArrayList()
    var names: ArrayList<String>
    val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return colors.size
    }

    override fun getItem(arg0: Int): Any {
        return colors[arg0]
    }

    override fun getItemId(arg0: Int): Long {
        return arg0.toLong()
    }

    override fun getView(pos: Int, view: View?, parent: ViewGroup?): View {
        val currView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, null)
        val textView = currView?.findViewById(android.R.id.text1) as TextView
        textView.setBackgroundColor(colors[pos])
        textView.textSize = 20f
        textView.text = names[pos]
        return currView
    }

    init {
        val colorArray = context.resources.getIntArray(R.array.category_colors)
        for (color in colorArray) {
            colors.add(color)
        }

        names = ArrayList()
        val nameArray = context.resources.getStringArray(R.array.category_color_names)
        for (name in nameArray) {
            names.add(name)
        }

    }
}