package com.example.testproject.utility

abstract class Constants {
    companion object {
        const val VIEW_TYPE_LIST_ITEM = 0
        const val VIEW_TYPE_FOOTER_ITEM = 1
    }
}