package com.example.testproject.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.testproject.R
import com.example.testproject.entity.Category
import com.example.testproject.utility.Constants
import com.example.testproject.utility.Constants.Companion.VIEW_TYPE_FOOTER_ITEM
import com.example.testproject.utility.Constants.Companion.VIEW_TYPE_LIST_ITEM

class SettingsCategoryRecyclerAdapter(private var categories: List<Category>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var settingsCategoryAdapterListener: SettingsCategoryAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when(viewType) {
            VIEW_TYPE_LIST_ITEM -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_category_dialog, parent, false)
                return ItemListViewHolder(rootView)
            }

            VIEW_TYPE_FOOTER_ITEM -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_footer, parent, false)
                return FooterViewHolder(rootView)
            }

            else -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_category_dialog, parent, false)
                return ItemListViewHolder(rootView)
            }
        }
    }

    //Add one more item to size to account for our footer view
    override fun getItemCount(): Int = categories.size + 1

    override fun getItemViewType(position: Int): Int {
        return if(position == categories.size) {
            VIEW_TYPE_FOOTER_ITEM
        } else {
            VIEW_TYPE_LIST_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is FooterViewHolder) {
            holder.itemListView.setOnClickListener { settingsCategoryAdapterListener?.onFooterClick() }
        } else if(holder is ItemListViewHolder) {
            val category = categories[position]
            holder.categoryName.text = category.name
            holder.categoryColor.setColorFilter(category.color)
            holder.itemListView.setOnClickListener { settingsCategoryAdapterListener?.onItemClick(category) }
        }
    }

    fun updateData(categories: List<Category>) {
        this.categories = categories
        notifyDataSetChanged()
    }

    inner class FooterViewHolder(val itemListView: View) : RecyclerView.ViewHolder(itemListView)

    inner class ItemListViewHolder(val itemListView: View) : RecyclerView.ViewHolder(itemListView) {
        var categoryName = itemListView.findViewById(R.id.category_name) as TextView
        var categoryColor = itemListView.findViewById(R.id.category_color) as ImageView
    }

    interface SettingsCategoryAdapterListener {
        fun onItemClick(category: Category)
        fun onFooterClick()
    }
}