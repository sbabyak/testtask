package com.example.testproject.di

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.testproject.TestProjectApp
import com.example.testproject.database.CategoriesDao
import com.example.testproject.database.TasksDao
import com.example.testproject.ui.*
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Singleton

/**
 * provide all the viewmodels using multibinding
 */
@Module
abstract class ViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainFragmentViewModel::class)
    abstract fun bindMainFragmentViewModel(viewModel: MainFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsFragmentViewModel::class)
    abstract fun bindSettingsFragmentViewModel(viewModel: SettingsFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TaskDetailsFragmentViewModel::class)
    abstract fun bindDetailsFragmentViewModel(viewModel: TaskDetailsFragmentViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}