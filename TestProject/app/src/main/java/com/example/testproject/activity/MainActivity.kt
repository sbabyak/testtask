package com.example.testproject.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.testproject.R
import com.example.testproject.entity.Task
import com.example.testproject.fragment.MainFragment
import com.example.testproject.fragment.SettingsFragment
import com.example.testproject.fragment.TaskDetailsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Activity only as a manager for fragments
 */
class MainActivity : AppCompatActivity(), MainFragment.MainFragmentInterface,
    TaskDetailsFragment.TaskDetailsFragmentInterface, HasSupportFragmentInjector {
    private var mainFragment: MainFragment? = null
    private var settingsFragment: SettingsFragment? = null
    private var toolbar: ActionBar? = null
    private var bottomNavigationView: BottomNavigationView? = null

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = supportActionBar
        bottomNavigationView = findViewById(R.id.navigation)
        bottomNavigationView?.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        loadMainFragment()
    }

    override fun taskSelected(task: Task) {
        replaceFragment(TaskDetailsFragment.newInstance(task))
    }

    override fun taskDetailsFinished() {
        loadMainFragment()
    }

    private fun loadMainFragment() {
        if(mainFragment == null) {
            mainFragment = MainFragment.newInstance()
        }

        replaceFragment(mainFragment!!)
    }

    override fun createTaskSelected() {
        replaceFragment(TaskDetailsFragment.newInstance())
    }

    private fun loadSettingsFragment() {
        if(settingsFragment == null) {
            settingsFragment = SettingsFragment.newInstance()
        }

        replaceFragment(settingsFragment!!)
    }

    private fun replaceFragment(fragment: Fragment) {
        when(fragment) {
            is MainFragment -> {
                toolbar?.title = resources.getString(R.string.title_tasks)
                bottomNavigationView?.visibility = View.VISIBLE
            }

            is SettingsFragment -> {
                bottomNavigationView?.visibility = View.VISIBLE
                toolbar?.title = resources.getString(R.string.title_settings)
            }

            is TaskDetailsFragment -> {
                bottomNavigationView?.visibility = View.GONE
                toolbar?.title = resources.getString(R.string.title_details)
            }
        }

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private val mOnNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.navigation_tasks -> {
                        loadMainFragment()
                        return true
                    }
                    R.id.navigation_settings -> {
                        loadSettingsFragment()
                        return true
                    }
                }
                return false
            }
        }
}
