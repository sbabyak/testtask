package com.example.testproject.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.testproject.R
import com.example.testproject.utility.Util
import com.example.testproject.entity.Task
import com.example.testproject.utility.Constants

/**
 *  There are 2 incredibly similar recycler adapters with footer capabilities that if given thought
 *  could be greatly optimized to avoid duplicating so much code
 **/
class TasksAdapter(val context: Context, private var tasks: List<Task>): RecyclerView.Adapter<RecyclerView.ViewHolder>(), ItemTouchHelperAdapter {
    var taskAdapterListener: TaskAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when(viewType) {
            Constants.VIEW_TYPE_LIST_ITEM -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_task, parent, false)
                return ListItemViewHolder(rootView)
            }

            Constants.VIEW_TYPE_FOOTER_ITEM -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_footer, parent, false)
                return FooterViewHolder(rootView)
            }

            else -> {
                val rootView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_task, parent, false)
                return ListItemViewHolder(rootView)
            }
        }
    }

    //Add 1 to size to account for footer view
    override fun getItemCount(): Int = tasks.size + 1

    fun refreshData(newTasks: List<Task>) {
        //We update tasks from our recycler adapter which will trigger database to refresh our data
        //which is actually identical
        if(newTasks == tasks) {
            return
        }

        tasks = newTasks
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if(position == tasks.size) {
            Constants.VIEW_TYPE_FOOTER_ITEM
        } else {
            Constants.VIEW_TYPE_LIST_ITEM
        }
    }

    override fun onItemDismiss(position: Int) {
        taskAdapterListener?.onItemDismiss(tasks[position])
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is FooterViewHolder) {
            holder.itemListView.setOnClickListener { taskAdapterListener?.onFooterClick() }
        } else if (holder is ListItemViewHolder) {
            val currTask = tasks[position]
            holder.taskName.text = currTask.name
            holder.taskCategoryColor.setColorFilter(currTask.category.color)
            holder.taskDate.text = Util.simpleDateFormat(currTask.date)
            holder.taskDone.isChecked = currTask.done

            holder.taskDone.setOnCheckedChangeListener { buttonView, isChecked ->
                if(!buttonView.isPressed) {
                    return@setOnCheckedChangeListener
                }

                currTask.done = isChecked
                taskAdapterListener?.onTaskDone(currTask)
                setDoneViewColorScheme(holder, isChecked)
            }

            holder.listItemView.setOnClickListener { taskAdapterListener?.onItemClick(currTask) }

            setDoneViewColorScheme(holder, currTask.done)
        }
    }

    //Change color scheme of our item depending on task completition
    private fun setDoneViewColorScheme(holder: ListItemViewHolder, isDone: Boolean) {
        val backgroundColor = if(isDone) {
            ContextCompat.getColor(context, R.color.grey_light)
        } else {
            ContextCompat.getColor(context, R.color.list_item_background)
        }
        holder.listItemView.setBackgroundColor(backgroundColor)
    }

    inner class FooterViewHolder(val itemListView: View) : RecyclerView.ViewHolder(itemListView)

    inner class ListItemViewHolder(val listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val taskName = listItemView.findViewById(R.id.task_name) as TextView
        val taskCategoryColor = listItemView.findViewById(R.id.task_category_color) as ImageView
        val taskDate = listItemView.findViewById(R.id.task_date) as TextView
        val taskDone = listItemView.findViewById(R.id.task_done) as CheckBox
    }

    //All adapters communicate with its creators via their unique listener interfaces
    interface TaskAdapterListener {
        fun onItemClick(task: Task)
        fun onItemDismiss(task: Task)
        fun onFooterClick()
        fun onTaskDone(task: Task)
    }

}