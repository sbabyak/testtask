package com.example.testproject.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.testproject.database.AppDatabase
import com.example.testproject.database.CategoriesDao
import com.example.testproject.database.TasksDao
import com.example.testproject.utility.Util
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Singleton
import javax.security.auth.callback.Callback


@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun providesDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "tasks.db"
        ).addCallback(object: RoomDatabase.Callback() {
            /*
                Lazy solution to pre-populating db. Ideally should be done with prepackaged file
                with createFromAsset()
             */
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                GlobalScope.launch {
                    providesDatabase(context).categoriesDao()
                        .insertCategories(Util.getBaseCategories(context))
                }
            }
        }).build()
    }

    @Provides
    @Singleton
    fun providesCategoriesDao(database: AppDatabase): CategoriesDao = database.categoriesDao()

    @Provides
    @Singleton
    fun providesTasksDao(database: AppDatabase): TasksDao = database.tasksDao()
}