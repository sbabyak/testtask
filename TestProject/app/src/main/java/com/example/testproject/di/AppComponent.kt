package com.example.testproject.di

import android.app.Application
import com.example.testproject.TestProjectApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class, AndroidInjectionModule::class, DatabaseModule::class,
        MainActivityModule::class, ViewModelsModule::class]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: TestProjectApp)
}
