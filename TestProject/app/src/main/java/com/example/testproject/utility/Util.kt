package com.example.testproject.utility

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.MotionEvent
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testproject.R
import com.example.testproject.adapter.DetailsCategoryPickerAdapter
import com.example.testproject.entity.Category
import com.example.testproject.entity.Task
import com.example.testproject.entity.TaskFilterEnum
import com.example.testproject.entity.TaskSortEnum
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

abstract class Util {
    companion object {
        fun simpleDateFormat(date: Date): String {
            val dateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            return dateFormat.format(date)
        }

        fun sortTasks(context: Context, taskList: List<Task>): List<Task> {
            val filteredTaskList = ArrayList<Task>()
            when(PreferencesHelper.getFilter(context)) {
                TaskFilterEnum.ALL -> {
                    return sortTaskByDateName(context, taskList)
                }
                TaskFilterEnum.DONE -> {
                    for (task in taskList) {
                        if(task.done) {
                            filteredTaskList.add(task)
                        }
                    }

                    return sortTaskByDateName(context, filteredTaskList)
                }

                TaskFilterEnum.NOT_DONE -> {
                    for (task in taskList) {
                        if(!task.done) {
                            filteredTaskList.add(task)
                        }
                    }

                    return sortTaskByDateName(context, filteredTaskList)
                }
            }
        }

        private fun sortTaskByDateName(context: Context, taskList: List<Task>): List<Task> {
            return when(PreferencesHelper.getTaskSort(context)) {
                TaskSortEnum.NAME -> sortTaskByName(taskList)
                TaskSortEnum.DATE -> sortTasksByDateASC(taskList)
            }
        }

        private fun sortTaskByName(taskList: List<Task>): List<Task> {
            val newTasks = ArrayList<Task>()
            var firstDone = 0
            for (task in taskList.sortedBy { it.name }) {
                if(task.done) {
                    newTasks.add(firstDone, task)
                    firstDone++
                } else {
                    newTasks.add(task)
                }
            }

            return newTasks
        }

        //Sort task list by date in ascending order in a way where tasks that have already been done
        //appear after those that are not
        private fun sortTasksByDateASC(taskList: List<Task>): List<Task> {
            val newTasks = ArrayList<Task>()
            var firstDone = 0
            for (task in taskList.sortedBy { it.date }) {
                if(task.done) {
                    newTasks.add(firstDone, task)
                    firstDone++
                } else {
                    newTasks.add(task)
                }
            }

            return newTasks
        }

        fun setupRecyclerView(context: Context, recyclerView: RecyclerView) {
            val layoutManager = object : LinearLayoutManager(context) {
                override fun supportsPredictiveItemAnimations(): Boolean {
                    return true
                }
            }

            recyclerView.itemAnimator = DefaultItemAnimator()
            recyclerView.layoutManager = layoutManager
            val dividerItemDecoration = DividerItemDecoration(recyclerView.context, layoutManager.orientation)
            recyclerView.addItemDecoration(dividerItemDecoration)
        }


        fun getBaseCategories(context: Context): List<Category> {
            val retCategories = ArrayList<Category>()
            val colors: IntArray = context.resources.getIntArray(R.array.category_colors)
            for((i, name) in context.resources.getStringArray(R.array.base_category_names).withIndex()) {
                retCategories.add(Category(name, colors[i]))
            }

            return retCategories
        }
    }

}

