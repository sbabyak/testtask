package com.example.testproject.adapter

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

/**
 * Interface to listen for a move or dismissal event from a [ItemTouchHelper.Callback].
 */
interface ItemTouchHelperAdapter {
    fun onItemDismiss(position: Int)
}