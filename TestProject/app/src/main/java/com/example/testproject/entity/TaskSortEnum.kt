package com.example.testproject.entity

enum class TaskSortEnum {
    NAME, DATE;

    companion object {
        private val values = values()
        fun getByValue(value: Int) = values.firstOrNull { it.ordinal == value }
    }
}