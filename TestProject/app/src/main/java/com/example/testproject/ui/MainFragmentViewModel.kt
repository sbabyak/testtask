package com.example.testproject.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.testproject.database.TasksDao
import com.example.testproject.entity.Task
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainFragmentViewModel @Inject constructor(private val tasksDao: TasksDao) : ViewModel() {

    var tasks: List<Task>? = null

    companion object {
        val TAG = MainFragmentViewModel::class.java.simpleName
    }

    fun getTasks(): LiveData<List<Task>> {
        Log.d(TAG, "getTasks called.")
        return tasksDao.getAllTasksLive()
    }

    fun updateTask(task: Task) {
        Log.d(TAG, "updateTasks called.")
        GlobalScope.launch {
            tasksDao.updateTask(task)
        }
    }

    fun updateTasks(tasks: List<Task>) {
        Log.d(TAG, "updateTasks called.")
        GlobalScope.launch {
            tasksDao.updateTasks(tasks)
        }
    }

    fun removeTask(task: Task) {
        Log.d(TAG, "deleteTask called.")
        GlobalScope.launch {
            tasksDao.removeTask(task)
        }
    }
}