package com.example.testproject.fragment

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.testproject.R
import com.example.testproject.adapter.DetailsCategoryPickerAdapter
import com.example.testproject.databinding.FragmentTaskDetailsBinding
import com.example.testproject.di.Injectable
import com.example.testproject.entity.Category
import com.example.testproject.entity.Task
import com.example.testproject.ui.TaskDetailsFragmentViewModel
import com.example.testproject.utility.Util
import com.example.testproject.utility.injectViewModel
import com.example.testproject.utility.withArgs
import kotlinx.android.synthetic.main.fragment_task_details.*
import java.util.*
import javax.inject.Inject

class TaskDetailsFragment: Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: TaskDetailsFragmentViewModel
    private lateinit var taskDetailsFragmentInterface: TaskDetailsFragmentInterface
    private var categoryAdapter: DetailsCategoryPickerAdapter = DetailsCategoryPickerAdapter(ArrayList())

    companion object {
        val TAG = TaskDetailsFragment::class.java.simpleName
        private const val ARG_TASK = "ARG_TASK"

        fun newInstance() = TaskDetailsFragment()

        fun newInstance(task: Task) = TaskDetailsFragment().withArgs {
            putParcelable(ARG_TASK, task)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is TaskDetailsFragmentInterface) {
            taskDetailsFragmentInterface = context
        } else {
            throw RuntimeException("To use $TAG, activity must implement OnFragmentInteractionListener!")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentTaskDetailsBinding.inflate(inflater, container, false)
        initViewModel()
        loadData()
        binding.taskViewModel = viewModel
        binding.lifecycleOwner = this.viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun loadData() {
        viewModel.getCategories().observe(this, Observer {
            categoryAdapter.updateData(it)

            if(arguments?.getParcelable<Task>(ARG_TASK) == null) {
                viewModel.task.value = Task("name", Date(), it[0], reminder = true, done = false)
                viewModel.updateData()
            } else {
                viewModel.task.value = arguments?.getParcelable(ARG_TASK)
                viewModel.updateData()
            }
        })
    }

    private fun setupViews() {
        create_update_task_button.setOnClickListener(getCallUpdateButtonOnclickListener())

        if(viewModel.createMode) {
            create_update_task_button.setText(R.string.add_new)
        }

        val calendar = Calendar.getInstance()
        if(viewModel.task.value != null) {
            calendar.time = viewModel.task.value!!.date
        }

        date_picker_details.setOnClickListener {
            getDatePickerDialog(calendar).show()
        }

        time_picker_details.setOnClickListener {
            getTimePickerDialog(calendar).show()
        }

        category_view.setOnClickListener {
            createCategoryPickerDialog(context!!, categoryAdapter!!).show()
        }

        task_details_cancel_button.setOnClickListener {
            taskDetailsFragmentInterface.taskDetailsFinished()
        }
    }

    private fun getDatePickerDialog(calendar: Calendar): DatePickerDialog {
        calendar.time = viewModel.task.value!!.date
        return DatePickerDialog(context!!,
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                calendar.set(year, month, dayOfMonth)
                viewModel.task.value?.date = calendar.time
                viewModel.updateData()
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
    }

    private fun getTimePickerDialog(calendar: Calendar): TimePickerDialog {
        calendar.time = viewModel.task.value!!.date
        return TimePickerDialog(context!!, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
           calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
               hourOfDay, minute)

            viewModel.task.value!!.date = calendar.time
            viewModel.updateData()
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true)
    }

    private fun getCallUpdateButtonOnclickListener() = View.OnClickListener {
        if(viewModel.task.value != null) {
            if(viewModel.createMode) {
                viewModel.insertTask(viewModel.task.value!!)
            } else {
                viewModel.updateTask(viewModel.task.value!!)
            }
        }

        taskDetailsFragmentInterface.taskDetailsFinished()
    }

    private fun createCategoryPickerDialog(
        context: Context, categoryAdapter: DetailsCategoryPickerAdapter
    ): AlertDialog {
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_custom_view, null)
        val dialog = AlertDialog.Builder(context)
            .setTitle(R.string.pick_category)
            .setView(dialogView)
            .setNegativeButton(android.R.string.no) { dialog, _ ->  dialog.dismiss() }
            .create()

        val recyclerView = dialogView.findViewById<RecyclerView>(R.id.dialog_recycler_view)
        categoryAdapter.settingsCategoryAdapterListener = object: DetailsCategoryPickerAdapter.SettingsCategoryAdapterListener {
            override fun onItemClick(category: Category) {
                viewModel.task.value?.category = category
                viewModel.updateData()
                dialog.dismiss()
            }
        }

        recyclerView.adapter = categoryAdapter
        Util.setupRecyclerView(context, recyclerView)
        return dialog
    }

    private fun initViewModel() {
        viewModel = injectViewModel(viewModelFactory)

        if(arguments?.getParcelable<Task>(ARG_TASK) == null) {
            viewModel.createMode = true
        }
    }

    interface TaskDetailsFragmentInterface {
        fun taskDetailsFinished()
    }
}