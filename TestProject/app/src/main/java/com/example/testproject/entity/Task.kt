package com.example.testproject.entity

import android.os.Parcel
import android.os.Parcelable
import android.view.View
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parceler
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "tasks")
@Parcelize
data class Task (
    @ColumnInfo(name = "name")
    var name: String,
    @ColumnInfo(name = "date")
    var date: Date,
    @Embedded(prefix = "task_")
    var category: Category,
    @ColumnInfo(name = "reminder")
    var reminder: Boolean,
    @ColumnInfo(name = "done")
    var done: Boolean
): Parcelable {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var taskId: Int = 0

    fun getFormattedTime(): String {
        return formatTaskDate("HH:mm")
    }

    fun getFormattedDate(): String {
        return formatTaskDate("dd.MM.yyyy")
    }

    private fun formatTaskDate(pattern: String): String {
        val formatter = SimpleDateFormat(pattern, Locale.getDefault())
        return formatter.format(date)
    }
}