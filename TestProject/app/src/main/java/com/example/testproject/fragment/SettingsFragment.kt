package com.example.testproject.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.example.testproject.R
import com.example.testproject.adapter.CategoryColorSpinnerAdapter
import com.example.testproject.adapter.SettingsCategoryRecyclerAdapter
import com.example.testproject.di.Injectable
import com.example.testproject.entity.Category
import com.example.testproject.entity.TaskFilterEnum
import com.example.testproject.entity.TaskSortEnum
import com.example.testproject.ui.SettingsFragmentViewModel
import com.example.testproject.utility.PreferencesHelper
import com.example.testproject.utility.Util
import com.example.testproject.utility.injectViewModel
import javax.inject.Inject

class SettingsFragment: Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var categoryAdapter: SettingsCategoryRecyclerAdapter
    private lateinit var viewModel: SettingsFragmentViewModel

    companion object {
        val TAG = SettingsFragment::class.java.simpleName

        fun newInstance() = SettingsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_settings, container, false)
        initViewModel()
        loadData()
        setupViews(rootView)
        return rootView
    }

    private fun initViewModel() {
        viewModel = injectViewModel(viewModelFactory)
    }

    private fun loadData() {
        viewModel.getCategories().observe(this, Observer {
            categoryAdapter.updateData(it)

            categoryAdapter.settingsCategoryAdapterListener = object: SettingsCategoryRecyclerAdapter.SettingsCategoryAdapterListener {
                override fun onItemClick(category: Category) {
                    getCreateUpdateDialog(category).show()
                }

                override fun onFooterClick() {
                    getCreateUpdateDialog(null).show()
                }
            }
        })
    }

    private fun setupViews(rootView: View) {
        val recyclerView = rootView.findViewById<RecyclerView>(R.id.settings_category_recycler)
        Util.setupRecyclerView(context!!, recyclerView)
        recyclerView.adapter = categoryAdapter
        val notifPrefCheckBox = rootView.findViewById<CheckBox>(R.id.pref_checkbox)

        if(PreferencesHelper.getNotifPref(context!!)) {
            notifPrefCheckBox.isChecked = true
        }

        notifPrefCheckBox.setOnCheckedChangeListener { _, isChecked ->
            PreferencesHelper.saveNotifPref(context!!, isChecked)
        }

        val taskFilterSpinner = rootView.findViewById<Spinner>(R.id.task_filter_spinner)
        setTaskFilterSpinnerAdapter(taskFilterSpinner)

        val taskSortSpinner = rootView.findViewById<Spinner>(R.id.task_sort_spinner)
        setTaskSortSpinnerAdapter(taskSortSpinner)
    }

    //Set simple spinner for our sorting options
    private fun setTaskSortSpinnerAdapter(spinner: Spinner) {
        //set simple spinner for our filtering options
        ArrayAdapter.createFromResource(
            context!!,
            R.array.task_sort_options,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(PreferencesHelper.getTaskSort(context!!).ordinal)
            spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    return
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    PreferencesHelper.saveTaskSort(context!!, TaskSortEnum.getByValue(position)!!)
                }
            }
        }
    }

    //Set simple spinner for our filtering options
    private fun setTaskFilterSpinnerAdapter(spinner: Spinner) {
        ArrayAdapter.createFromResource(
            context!!,
            R.array.task_filter_options,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(PreferencesHelper.getFilter(context!!).ordinal)
            spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    return
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    PreferencesHelper.saveFilter(context!!, TaskFilterEnum.getByValue(position)!!)
                }
            }
        }
    }

    private fun getCreateUpdateDialog(category: Category?): AlertDialog {
        val title = if(category == null) R.string.create_category else R.string.edit
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_create_category, null, false)
        val nameEditText = dialogView.findViewById<EditText>(R.id.name_edit)
        val spinner = dialogView.findViewById<Spinner>(R.id.color_spinner)
        val spinnerAdapter = CategoryColorSpinnerAdapter(context!!)
        spinner.adapter = spinnerAdapter
        val colorIntArray = resources.getIntArray(R.array.category_colors)
        if(category != null) {
            nameEditText.setText(category.name)
            spinner.setSelection(findCategoryColorIndex(category, colorIntArray))
        }

        resources.getIntArray(R.array.category_colors)

        return AlertDialog.Builder(context)
            .setTitle(title)
            .setView(dialogView)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                if(nameEditText.text.isEmpty()) {
                    return@setPositiveButton
                }

                if(category != null) {
                    category.name = nameEditText.text.toString()
                    category.color = colorIntArray[spinner.selectedItemPosition]
                    viewModel.updateCategory(category)
                } else {
                    viewModel.insertCategory(Category(nameEditText.text.toString()
                        , colorIntArray[spinner.selectedItemPosition]))
                }

                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
    }

    private fun findCategoryColorIndex(category: Category, intArray: IntArray): Int {
        for((i, color) in intArray.withIndex()) {
            if(color == category.color) {
                return i
            }
        }

        return 0
    }
}