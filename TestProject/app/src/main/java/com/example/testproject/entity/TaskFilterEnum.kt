package com.example.testproject.entity

enum class TaskFilterEnum {
    ALL, DONE, NOT_DONE;

    companion object {
        private val values = values()
        fun getByValue(value: Int) = values.firstOrNull { it.ordinal == value }
    }
}