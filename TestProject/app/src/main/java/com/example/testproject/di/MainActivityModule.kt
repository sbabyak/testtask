package com.example.testproject.di

import com.example.testproject.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule{
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeActivityInjector(): MainActivity
}