package com.example.testproject.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.testproject.entity.Category

@Dao
abstract class CategoriesDao {
    @Query("SELECT * FROM categories")
    abstract fun getAllCategories(): List<Category>

    @Query("SELECT * FROM categories")
    abstract fun getAllCategoriesLive(): LiveData<List<Category>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertCategory(category: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCategories(categories: List<Category>)

    @Update
    abstract fun updateCategory(category: Category)
}