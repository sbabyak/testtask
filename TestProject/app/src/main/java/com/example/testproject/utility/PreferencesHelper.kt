package com.example.testproject.utility

import android.app.Activity
import android.content.Context
import android.preference.PreferenceManager
import com.example.testproject.entity.TaskFilterEnum
import com.example.testproject.entity.TaskSortEnum

abstract class PreferencesHelper {
    companion object {
        const val PREF_NOTIF = "PREF_NOTIF"
        const val PREF_FILTER = "PREF_FILTER"
        const val PREF_SORT = "PREF_SORT"
        const val SHARED_PREF = "SHARED_PREF"

        fun saveFilter(context: Context, filterEnum: TaskFilterEnum) {
            val editor = context.getSharedPreferences(SHARED_PREF, Activity.MODE_PRIVATE).edit()
            editor.putInt(PREF_FILTER, filterEnum.ordinal)
            editor.apply()
            editor.commit()
        }

        fun saveTaskSort(context: Context, sortEnum: TaskSortEnum) {
            val editor = context.getSharedPreferences(SHARED_PREF, Activity.MODE_PRIVATE).edit()
            editor.putInt(PREF_SORT, sortEnum.ordinal)
            editor.apply()
            editor.commit()
        }

        fun getTaskSort(context: Context): TaskSortEnum {
            val pref = context.getSharedPreferences(SHARED_PREF, Activity.MODE_PRIVATE)
            return TaskSortEnum.getByValue(pref.getInt(PREF_SORT, 0))!!
        }

        fun getFilter(context: Context): TaskFilterEnum {
            val pref = context.getSharedPreferences(SHARED_PREF, Activity.MODE_PRIVATE)
            return TaskFilterEnum.getByValue(pref.getInt(PREF_FILTER, 0))!!
        }

        fun saveNotifPref(context: Context, prefEnabled: Boolean) {
            val editor = context.getSharedPreferences(SHARED_PREF, Activity.MODE_PRIVATE).edit()
            editor.putBoolean(PREF_NOTIF, prefEnabled)
            editor.apply()
            editor.commit()
        }

        fun getNotifPref(context: Context): Boolean {
            val pref = context.getSharedPreferences(SHARED_PREF, Activity.MODE_PRIVATE)
            return pref.getBoolean(PREF_NOTIF, true)
        }

    }


}