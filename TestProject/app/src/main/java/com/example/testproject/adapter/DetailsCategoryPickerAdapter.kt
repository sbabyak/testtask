package com.example.testproject.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.testproject.R
import com.example.testproject.entity.Category

class DetailsCategoryPickerAdapter(private var categories: List<Category>): RecyclerView.Adapter<DetailsCategoryPickerAdapter.ViewHolder>() {
    var settingsCategoryAdapterListener: SettingsCategoryAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rootView = LayoutInflater.from(parent.context).inflate(R.layout.list_item_category_recycler, parent, false)
        return ViewHolder(rootView)
    }

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = categories[position]
        holder.categoryName.text = category.name
        holder.categoryColor.setColorFilter(category.color)
        holder.itemListView.setOnClickListener { settingsCategoryAdapterListener?.onItemClick(category) }
    }

    fun updateData(newList: List<Category>) {
        categories = newList
    }

    inner class ViewHolder(val itemListView: View) : RecyclerView.ViewHolder(itemListView) {
        var categoryName = itemListView.findViewById(R.id.category_name) as TextView
        var categoryColor = itemListView.findViewById(R.id.category_color) as ImageView
    }

    interface SettingsCategoryAdapterListener {
        fun onItemClick(category: Category)
    }
}