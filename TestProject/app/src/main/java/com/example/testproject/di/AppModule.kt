package com.example.testproject.di

import android.app.Application
import android.content.Context
import com.example.testproject.adapter.SettingsCategoryRecyclerAdapter
import com.example.testproject.adapter.TasksAdapter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app

    @Provides
    fun provideTaskAdapter(context: Context): TasksAdapter = TasksAdapter(context, ArrayList())

    @Provides
    fun provideCategoryRecyclerAdapter(): SettingsCategoryRecyclerAdapter = SettingsCategoryRecyclerAdapter(ArrayList())
}