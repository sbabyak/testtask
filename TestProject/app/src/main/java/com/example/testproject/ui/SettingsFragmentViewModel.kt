package com.example.testproject.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.testproject.database.CategoriesDao
import com.example.testproject.entity.Category
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class SettingsFragmentViewModel @Inject constructor(private val categoriesDao: CategoriesDao) : ViewModel() {

    companion object {
        val TAG = SettingsFragmentViewModel::class.java.simpleName
    }

    fun getCategories(): LiveData<List<Category>> {
        Log.d(TAG, "getCategories called")
        return categoriesDao.getAllCategoriesLive()
    }

    fun insertCategory(category: Category) {
        Log.d(TAG, "insertCategory called")
        GlobalScope.launch {
            categoriesDao.insertCategory(category)
        }
    }

    fun updateCategory(category: Category) {
        Log.d(TAG, "updateCategory called")
        GlobalScope.launch {
            categoriesDao.updateCategory(category)
        }
    }
}