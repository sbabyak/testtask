package com.example.testproject.ui

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testproject.database.CategoriesDao
import com.example.testproject.database.TasksDao
import com.example.testproject.entity.Category
import com.example.testproject.entity.Task
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class TaskDetailsFragmentViewModel @Inject constructor(
    private val tasksDao: TasksDao,
    private val categoriesDao: CategoriesDao
) : ViewModel() {

    val task = MutableLiveData<Task>()
    var createMode: Boolean = false

    companion object {
        val TAG = TaskDetailsFragmentViewModel::class.java.simpleName
    }

    fun updateTask(task: Task) {
        Log.d(TAG, "updateTask called.")
        GlobalScope.launch {
            tasksDao.updateTask(task)
        }
    }

    fun insertTask(task: Task) {
        Log.d(TAG, "insertTask called.")
        GlobalScope.launch {
            tasksDao.insertTask(task)
        }
    }

    fun getCategories(): LiveData<List<Category>> {
        Log.d(TAG, "getCategories called.")
        return categoriesDao.getAllCategoriesLive()
    }

    fun updateData() {
        task.postValue(task.value)
    }

    fun getEditModeViewsVisibility(): Int {
        return if(createMode) View.GONE else View.VISIBLE
    }
}