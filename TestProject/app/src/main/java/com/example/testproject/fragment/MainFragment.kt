package com.example.testproject.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.testproject.utility.Util
import com.example.testproject.adapter.TasksAdapter
import com.example.testproject.databinding.FragmentMainBinding
import com.example.testproject.di.Injectable
import com.example.testproject.entity.Task
import com.example.testproject.ui.MainFragmentViewModel
import com.example.testproject.utility.injectViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

class MainFragment: Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainFragmentViewModel
    private lateinit var mainFragmentInterface: MainFragmentInterface
    @Inject
    lateinit var listAdapter: TasksAdapter

    companion object {
        val TAG = MainFragment::class.java.simpleName

        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainFragmentInterface) {
            mainFragmentInterface = context
        } else {
            throw RuntimeException("To use $TAG, activity must implement OnFragmentInteractionListener!")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentMainBinding.inflate(inflater, container, false)
        initViewModel()
        loadData()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }


    //All fragments use MVVP pattern to abstract themselves from data they are using
    private fun initViewModel() {
        viewModel = injectViewModel(viewModelFactory)
    }

    private fun setupViews() {
        Util.setupRecyclerView(context!!, recyclerView)
        listAdapter.taskAdapterListener = getTaskRecyclerListener()
        recyclerView.adapter = listAdapter
        check_all.setOnCheckedChangeListener(getCheckAllOnCheckedListener())
    }

    //Check all listener to mark all tasks as done/not done
    private fun getCheckAllOnCheckedListener() = CompoundButton.OnCheckedChangeListener { _, isChecked ->
        if (viewModel.tasks == null) {
            return@OnCheckedChangeListener
        }

        val newList = ArrayList<Task>()
        if (isChecked) {
            for (task in viewModel.tasks!!) {
                task.done = true
                newList.add(task)
            }
        } else {
            for (task in viewModel.tasks!!) {
                task.done = false
                newList.add(task)
            }
        }

        viewModel.updateTasks(newList)
    }

    private fun getTaskRecyclerListener() = object : TasksAdapter.TaskAdapterListener {
        override fun onItemClick(task: Task) {
            mainFragmentInterface.taskSelected(task)
        }

        override fun onItemDismiss(task: Task) {
            viewModel.removeTask(task)
        }

        override fun onFooterClick() {
            mainFragmentInterface.createTaskSelected()
        }

        override fun onTaskDone(task: Task) {
            viewModel.updateTask(task)
        }
    }

    private fun loadData() {
        viewModel.getTasks().observe(this, Observer {
            if(it != null) {
                viewModel.tasks = it
                listAdapter.refreshData(Util.sortTasks(context!!, it))
            }
        })
    }

    interface MainFragmentInterface {
        fun taskSelected(task: Task)
        fun createTaskSelected()
    }
}