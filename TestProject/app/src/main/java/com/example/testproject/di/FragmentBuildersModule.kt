package com.example.testproject.di

import com.example.testproject.fragment.MainFragment
import com.example.testproject.fragment.SettingsFragment
import com.example.testproject.fragment.TaskDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailsFragment(): TaskDetailsFragment
}