package com.example.testproject.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.testproject.entity.Category
import com.example.testproject.entity.Task

@Database(entities = [Task::class, Category::class], version = 1)
@TypeConverters(RoomDateConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun tasksDao(): TasksDao
    abstract fun categoriesDao(): CategoriesDao

    companion object {
        var TEST_MODE = false
        private var INSTANCE: AppDatabase? = null

        //getting a singleton instance of AppDatabase used for Testing
        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = if (TEST_MODE) {
                        Room.inMemoryDatabaseBuilder(context,
                            AppDatabase::class.java).allowMainThreadQueries().build()
                    } else {
                        Room.databaseBuilder(context.applicationContext,
                            AppDatabase::class.java, "test.db").build()

                    }
                }
            }

            return INSTANCE
        }

        private fun close() {
            INSTANCE?.close()
            INSTANCE = null
        }
    }
}