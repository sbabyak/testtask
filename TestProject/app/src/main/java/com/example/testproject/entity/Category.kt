package com.example.testproject.entity

import android.graphics.Color
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Entity(tableName = "categories")
@Parcelize
data class Category(
    @ColumnInfo(name = "name")
    var name: String,
    @ColumnInfo(name = "color")
    var color: Int
): Parcelable {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var categoryId: Int = 0
}