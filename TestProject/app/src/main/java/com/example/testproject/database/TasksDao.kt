package com.example.testproject.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.testproject.entity.Task

@Dao
abstract class TasksDao {
    @Query("SELECT * FROM tasks")
    abstract fun getAllTasksLive(): LiveData<List<Task>>

    @Query("SELECT * FROM tasks")
    abstract fun getAllTasks(): List<Task>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTask(task: Task)

    @Update
    abstract fun updateTask(task: Task)

    @Update
    abstract fun updateTasks(tasks: List<Task>)

    @Delete
    abstract fun removeTask(task: Task)
}